import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Operation } from '../entity/operation';
import { OperationService } from '../services/operation.service';

@Component({
  selector: 'app-vente',
  templateUrl: './vente.component.html',
  styleUrls: ['./vente.component.css']
})
export class VenteComponent implements OnInit {


  operations: Observable<Operation[]>;
  newOperation: Operation = {name:"", price:null, type:false , category:"", date:null};
  selected: Operation;

  constructor(private service:OperationService) { }

  ngOnInit() {
    this.operations = this.service.findAll();
  }


  isPassed(date:Date) {
    let now = new Date();
    
    return date.getTime() < now.getTime();
  }

}
