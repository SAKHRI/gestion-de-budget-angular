
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabAchatComponent } from './tab-achat.component';

describe('TabAchatComponent', () => {
  let component: TabAchatComponent;
  let fixture: ComponentFixture<TabAchatComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TabAchatComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TabAchatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
