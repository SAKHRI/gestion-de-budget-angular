import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TabAchatDataSource } from './tab-achat-datasource';
import { OperationService } from '../services/operation.service';

@Component({
  selector: 'app-tab-achat',
  templateUrl: './tab-achat.component.html',
  styleUrls: ['./tab-achat.component.css']
})
export class TabAchatComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TabAchatDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'date', 'name', 'price'];

  constructor(private service:OperationService){}

  ngOnInit() {
    this.service.findAll().subscribe(data => this.dataSource = new TabAchatDataSource(this.paginator, this.sort, data))
    ;
  }
}
