import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  nombre1: number = 0;
  nombre2: number = 0;
  resultat: number = 0;



  constructor() { }

  ngOnInit() {
  }


  addition(resultat) {

    this.resultat = this.nombre1 + this.nombre2;
    return (resultat);
  }


  multiplication(resultat) {

    this.resultat = this.nombre1 * this.nombre2;
    return (resultat);
  }


  division(resultat) {

    this.resultat = this.nombre1 / this.nombre2;
    return (resultat);
  }



}
