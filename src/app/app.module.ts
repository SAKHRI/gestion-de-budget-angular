import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes, Router } from '@angular/router';

import { AppComponent } from './app.component';
import { AchatComponent } from './achat/achat.component';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabCompteComponent } from './tab-compte/tab-compte.component';
import { CompteComponent } from './compte/compte.component';
import { VenteComponent } from './vente/vente.component';
import { TabAchatComponent } from './tab-achat/tab-achat.component';
import { TabVenteComponent } from './tab-vente/tab-vente.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';




const routes: Routes = [
  
  { path: "", component: HomeComponent },
  { path: "compte", component: CompteComponent},
  { path: "achat", component: AchatComponent},
  { path: "vente", component: VenteComponent}
  
]

@NgModule({
  declarations: [
    AppComponent,
    AchatComponent,
    MyNavComponent,
    TabCompteComponent,
    CompteComponent,
    VenteComponent,
    TabAchatComponent,
    TabVenteComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
