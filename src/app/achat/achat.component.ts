import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Operation } from '../entity/operation';
import { OperationService } from '../services/operation.service';

@Component({
  selector: 'app-achat',
  templateUrl: './achat.component.html',
  styleUrls: ['./achat.component.css']
})
export class AchatComponent implements OnInit {

  operations: Observable<Operation[]>;
  newOperation: Operation = {name:"", price:null, type: true , category:"", date:null};
  selected: Operation;



  constructor(private service:OperationService) { }

  ngOnInit() {
    this.operations = this.service.findAll();
  }


  isPassed(date:Date) {
    let now = new Date();
    
    return date.getTime() < now.getTime();
  }

}
