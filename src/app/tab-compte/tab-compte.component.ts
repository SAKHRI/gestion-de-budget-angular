import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TabCompteDataSource } from './tab-compte-datasource';

@Component({
  selector: 'app-tab-compte',
  templateUrl: './tab-compte.component.html',
  styleUrls: ['./tab-compte.component.css']
})
export class TabCompteComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TabCompteDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'date', 'achats', 'ventes', 'total'];

  ngOnInit() {
    this.dataSource = new TabCompteDataSource(this.paginator, this.sort);
  }
}
