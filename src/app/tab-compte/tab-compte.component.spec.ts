
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabCompteComponent } from './tab-compte.component';

describe('TabCompteComponent', () => {
  let component: TabCompteComponent;
  let fixture: ComponentFixture<TabCompteComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TabCompteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TabCompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
