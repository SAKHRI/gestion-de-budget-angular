export interface Category {


  id?: number;
  name: string;
  operations: number;
  image: string;

}
