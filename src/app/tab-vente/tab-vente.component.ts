import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TabVenteDataSource } from './tab-vente-datasource';
import { OperationService } from '../services/operation.service';

@Component({
  selector: 'app-tab-vente',
  templateUrl: './tab-vente.component.html',
  styleUrls: ['./tab-vente.component.css']
})
export class TabVenteComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TabVenteDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'date', 'name', 'price'];


  constructor(private service:OperationService){}

  ngOnInit() {
    this.service.findAll().subscribe(data => this.dataSource = new TabVenteDataSource(this.paginator, this.sort, data))
    ;
    
  }
}


