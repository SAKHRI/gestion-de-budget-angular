
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabVenteComponent } from './tab-vente.component';

describe('TabVenteComponent', () => {
  let component: TabVenteComponent;
  let fixture: ComponentFixture<TabVenteComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TabVenteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TabVenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
